# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

* guide install
1. install yarn and foreman
  gem install foreman
  yarn install
2. update libaray
  yarn add react-router react-router-dom webpack-cli resolve-url-loader
  yarn upgrade webpack-dev-server@^2.11.1 -D
3. start server
  foreman start -f Procfile.dev -p 3000
