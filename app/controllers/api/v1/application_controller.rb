module Api
  module V1
    class ApplicationController < ActionController::API
      rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

      private

      def record_not_found(error)
        render_error(
          http_status: 404,
          error_code: "E00001",
          messages: I18n.t("api.v1.errors.messages.record_not_found")
        )
      end

      def render_error(http_status:, error_code:, messages:)
        render json: {
          status: :error,
          code: error_code,
          errors: Array(messages)
        }, status: http_status
      end
    end
  end
end
