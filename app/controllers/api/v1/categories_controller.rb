module Api
  module V1
    class CategoriesController < Api::V1::ApplicationController
      before_action :load_category, only: %i(show update)

      def index
        @categories = Category.all
        render json: @categories
      end
      
      def create
        @category = Category.new(category_params)
        if @category.save
          render json: @category
        else
          render_error(
            http_status: :unprocessable_entity,
            error_code: "E00002",
            messages: @category.errors.messages
          )
        end
      end
      
      def show
        render json: @category
      end

     def update
      if @category.update_attributes(category_params)
        render json: @category
      else
        render_error(
          http_status: :unprocessable_entity,
          error_code: "E00002",
          messages: @category.errors.messages
        )
      end
     end
     
    private

    def category_params 
      params.require(:category).permit(:name, :slug)
    end

    def load_category
      @category = Category.find(params[:id])
    end
  end
end
