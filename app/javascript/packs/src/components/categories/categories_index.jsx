import React from "react"
import PropTypes from "prop-types"
export default class categoriesIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    };
  }

  componentDidMount(){
    fetch('/api/v1/categories.json')
      .then((response) => {return response.json()})
        .then((data) => {this.setState({ categories: data }) });
  }

  render () {
    var categories = this.state.categories.map((category) => {
      return (
        <tr key={category.id}>
          <td>
            <a>{category.name}</a>
          </td>
          <td>{category.slug}</td>
        </tr>
      )
    })

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Slug</th>
          </tr>
        </thead>
        <tbody>
          {categories}
        </tbody>
      </table>
    )
  }
}
