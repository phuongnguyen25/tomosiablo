import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import LandingPage from './components/landingPage';
import CategoriesIndex from './components/categories/categories_index'

const App = (props) => (
  <Router>
    <div>
      <Route exact path='/' component={LandingPage} />
      <Route exact path='/categories' component={CategoriesIndex} />
    </div>
  </Router>
)
export default App;
