class Category < ApplicationRecord
  has_many :posts, dependent: :destroy

  validates :name, presence: true, length: {maximum: 255}
  validates :slug, presence: true, length: {maximum: 50}
end
