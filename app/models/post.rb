class Post < ApplicationRecord
  belongs_to :user
  belongs_to :category

  has_many :comments, dependent: :destroy
  has_many :topic_posts, dependent: :destroy
  has_many :topic, through: :topic_posts
  has_many :post_tags, dependent: :destroy 
  has_many :tags, through: :post_tags

  validates :title, presence: true, length: {maximum: 255}
  validates :description, presence: true, length: {maximum: 255}
  validates :image, presence: true
  validates :content, presence: true, length: {maximum: 500}
  validates :slug, presence: true, length: {maximum: 50}
end
