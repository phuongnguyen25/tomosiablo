class Role < ApplicationRecord
  has_many :users

  validates :name, presence: true, length: {maximum: 25}
end
