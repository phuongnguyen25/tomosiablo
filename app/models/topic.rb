class Topic < ApplicationRecord
  belongs_to :user

  has_many :topic_posts
  has_many :posts, through: :topic_posts

  validates :name, presence: true, length: {maximum: 255}
end
