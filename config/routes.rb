Rails.application.routes.draw do
  root to: "pages#root"

  namespace :api do
    namespace :v1 do
      resources :categories, except: [:destroy, :new]
    end
  end

  match "*path", to: "pages#root", via: :all
end
