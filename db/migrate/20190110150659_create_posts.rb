class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title, null: false 
      t.string :description, null: false
      t.string :image, null: false  
      t.text :content, null: false
      t.string :slug, null: false
      t.integer :view, null: false 
      t.boolean :status, default: true
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
