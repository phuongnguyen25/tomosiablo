class CreateTopicPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :topic_posts do |t|
      t.references :topic, foreign_key: true
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
