FactoryBot.define do
  factory :category do
    name { "MyString" }
    slug { "MyString" }
    status { false }
  end
end
