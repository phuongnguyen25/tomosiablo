FactoryBot.define do
  factory :tag do
    name { "MyString" }
    slug { "MyString" }
    status { false }
  end
end
