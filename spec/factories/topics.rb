FactoryBot.define do
  factory :topic do
    name { "MyString" }
    status { 1 }
    user { nil }
  end
end
