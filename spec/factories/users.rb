FactoryBot.define do
  factory :user do
    name { "MyString" }
    phone { "MyString" }
    role { nil }
  end
end
